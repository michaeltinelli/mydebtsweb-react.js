import React from 'react'
import { Route, Switch, BrowserRouter as Router, Redirect  } from "react-router-dom";
import SaveProduct from '../pages/SaveProduct'
import SavePerson from '../pages/SavePerson';
import Products from '../pages/Products'
import People from '../pages/People'
import App from '../App'
import TopBar from '../templates/TopBar'
import Debts from '../pages/Debts';
import SaveDebt from '../pages/SaveDebt';

export default props => (
    <Router >
        <TopBar />
        <Switch>
            <Route exact path="/" component={App} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/people" component={People} />
            <Route exact path={`/product/:productId?`} component={(props) => <SaveProduct {...props} isEdit={true} header={'Editar Produto'} />} />
            <Route exact path="/products/newProduct" component={(props) => <SaveProduct {...props} isEdit={false} header={'Novo Produto'} />} />
            <Route exact path={"/people/newperson"} component={(props) => <SavePerson {...props} isEdit={false} header={'Nova Pessoa'} />} />
            <Route exact path={"/people/:personId?"} component={(props) => <SavePerson {...props} isEdit={true} header={'Editar Pessoa'} />} />
            <Route exact path="/debts" component={Debts} />
            <Route exact path={"/debts/newdebt"} component={(props) => <SaveDebt {...props} isEdit={false} header={'Nova Dívida'} />} />
            <Redirect from='*' to='/' />
        </Switch>
    </Router>
)