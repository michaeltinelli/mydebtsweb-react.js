import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import prodReducer from '../store/reducers/productReducer'
import peopleReducer from '../store/reducers/peopleReducer'
import debtsReducer from '../store/reducers/debtsReducer'

export default combineReducers({
    form: formReducer,
    prod: prodReducer,
    people: peopleReducer,
    debts: debtsReducer,
})
