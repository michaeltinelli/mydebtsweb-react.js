import { NEW_CATEGORY, LIST_CATEGORIES, LIST_PRODUCTS, NEW_PRODUCT, PRODUCT_REMOVED, PRODUCT_LOADED } from "../actionTypes/actionTypes";


const initialValues = {
    products: [],
    categories: [],
    product: null,
}

export default (state = initialValues, action) => {
    switch (action.type) {
        case NEW_CATEGORY:
           return {
               ...state,
               categories: state.categories.concat({...action.payload})
           }
        case LIST_CATEGORIES:
            return {
                ...state, categories: action.payload
            }
        case NEW_PRODUCT:
            return {
                ...state,
                products: state.products.concat({...action.payload}),
            }
        case LIST_PRODUCTS:
             return {
                 ...state, products: action.payload
             } 
        case PRODUCT_REMOVED:
            return {
                ...state,
                products: state.products.filter(p => p.id !== action.payload),
            }
        case PRODUCT_LOADED:
           
            return {
                ...state,
                product: {...action.payload}
            }                     
        default:
           return state;
    }
}