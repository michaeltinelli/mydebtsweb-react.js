import { NEW_PERSON, LIST_PEOPLE, PERSON_LOADED } from "../actionTypes/actionTypes";

const initialValues = {
   person: null,
   people: []
}

export default (state = initialValues, action) => {
    switch (action.type) {
        case NEW_PERSON:
            return {
                ...state,
                people: state.people.concat({ ...action.payload })
            } 
        case LIST_PEOPLE:
            return {
                ...state,
                people: action.payload
            }
        case PERSON_LOADED:
            return {
                ...state,
                person:  action.payload
            }                            
        default:
           return state;
    }
}