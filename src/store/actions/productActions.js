import * as firebase from 'firebase/app'
import { NEW_PRODUCT, LIST_PRODUCTS, PRODUCT_LOADED, PRODUCT_REMOVED } from '../actionTypes/actionTypes'
import { DEBTSWEB } from '../collections'
import { initialize } from 'redux-form'

//const debtsRef = firebase.firestore().collection(DEBTSWEB)



export function newProduct(product) {
    //console.log(product)
    return (dispatch) => {
        firebase.firestore().collection(DEBTSWEB).add(product)
            .then(resp => {
                resp.get().then(r => {
                    const prod = {
                        id: resp.id,
                        name: r.data().name,
                        description: r.data().description,
                        category: r.data().category,
                        price: r.data().price,
                        updateAt: r.data().updateAt   
                    }
                    dispatch({type: NEW_PRODUCT, payload: prod })
                })
            })
    }
}

export function loadProduct(id) {
   
    return dispatch => {
        firebase.firestore().collection(DEBTSWEB).doc(id).get().then(resp => {
            const prod = {
                id: resp.id,
                name: resp.data().name,
                description: resp.data().description,
                category: resp.data().category,
                price: resp.data().price,
                updateAt: resp.data().updateAt   
            }
            dispatch(initialize('SaveProductForm',  {...prod} ))  
        })
    }
}

export function removeProduct(id) {
    return dispatch => {
        firebase.firestore().collection(DEBTSWEB).doc(id).delete()
            .then(_ => dispatch({type: PRODUCT_REMOVED, payload: id}))
    }
}

export function updateProduct(product) {
    return dispatch => {
        firebase.firestore().collection(DEBTSWEB).doc(product.id).set({...product})
        .then(_ => dispatch({type: PRODUCT_LOADED, payload: product}))
    }
}

export function listProducts() {
    return (dispatch) => {
        firebase.firestore().collection(DEBTSWEB).orderBy('updateAt', 'desc').get().then(resp => {
           const list = resp.docs.map(r => {
            const prod = {
                id: r.id,
                name: r.data().name,
                description: r.data().description,
                category: r.data().category,
                price: r.data().price,
                updateAt: r.data().updateAt   
            }
               return prod
           })
           dispatch({type: LIST_PRODUCTS, payload: list })
        })
    }
}