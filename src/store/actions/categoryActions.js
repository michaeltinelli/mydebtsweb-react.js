import * as firebase from 'firebase/app'
import { NEW_CATEGORY, LIST_CATEGORIES } from '../actionTypes/actionTypes'
import { CATEGORIES } from '../collections'

const categoriesRef = firebase.firestore().collection(CATEGORIES)

export function newCategory(category) {
    console.log(category)
    return (dispatch) => {
        categoriesRef.add(category)
            .then(resp => {
                resp.get().then(r => {
                    const cat = {
                        id: resp.id,
                        name: r.data().name   
                    }
                    dispatch({type: NEW_CATEGORY, payload: cat })
                })
            })
            .catch(err => console.error(err))
    }
}

export function listCategories() {
    return (dispatch) => {
        categoriesRef.get().then(cats => {
           const list = cats.docs.map(c => {
               const cat = {
                   id: c.id,
                   name: c.data().name
               }
               return cat
           })
           dispatch({type: LIST_CATEGORIES, payload: list })
        })
    }
}