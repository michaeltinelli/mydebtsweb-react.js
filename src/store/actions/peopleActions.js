import * as firebase from 'firebase/app'
import { PEOPLE } from '../collections'
import { initialize } from 'redux-form'
import { NEW_PERSON, LIST_PEOPLE, PRODUCT_LOADED, PERSON_LOADED } from '../actionTypes/actionTypes';



export function newPerson(person) {
    

    return (dispatch) => {
        firebase.firestore().collection(PEOPLE).add(person)
            .then(resp => {
                resp.get().then(r => {
                    const per = {
                        id: resp.id,
                        name: r.data().name,
                        cel: r.data().cel,
                        email: r.data().email,
                        valueOfDebts: 0,
                        createdAt: r.data().createdAt,  
                    }
                    dispatch({type: NEW_PERSON, payload: per })
                })
            })
    }
}

export function loadPeople() {
    return (dispatch) => {
        firebase.firestore().collection(PEOPLE).orderBy('createdAt', 'desc').get()
            .then(resp => {
                const list = resp.docs.map(p => {
                    const person = {
                        id: p.id,
                        name: p.data().name,
                        cel: p.data().cel,
                        email: p.data().email,
                        createdAt: p.data().createdAt,
                        valueOfDebts: 0
                    }
                    return person
                })
                dispatch({type: LIST_PEOPLE, payload: list})
            })
    }
}

export function loadPerson(id) {
    return dispatch => {
        firebase.firestore().collection(PEOPLE).doc(id).get()
            .then(resp => {
                const person = {
                    id: resp.id,
                    name: resp.data().name,
                    cel: resp.data().cel,
                    email: resp.data().email,
                    createdAt: resp.data().createdAt,
                }
                dispatch(initialize('SavePersonForm',  {...person} ))  
            })
    }
}

export function updatePerson(person) {
    console.log(person)
    return dispatch => {
        firebase.firestore().collection(PEOPLE).doc(person.id).set({...person})
            .then(_ => dispatch({type: PERSON_LOADED, payload: person}))
    }
}
