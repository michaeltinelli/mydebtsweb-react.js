import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css'

import { Provider } from 'react-redux'
import { createStoreWithFirebase } from './config/mainConfig'
import rootReducer from './main/reducers'
import Routes from './main/routes'

const store = createStoreWithFirebase(rootReducer)

ReactDOM.render(
<Provider store={store}>
    <Routes />
</Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
