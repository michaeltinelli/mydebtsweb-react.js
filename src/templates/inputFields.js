import React from 'react'
import CurrencyInput from 'react-currency-input';
import { InputText } from 'primereact/inputtext'
import {InputTextarea} from 'primereact/inputtextarea';
import {Dropdown} from 'primereact/dropdown';
import {Message} from 'primereact/message';
import {InputMask} from 'primereact/inputmask';


export const InputDefault = ({onShowError, size, input, label, meta: { touched, error }, })  => (
    <div>
        
        <label htmlFor="in">{label}</label><br />
        <InputText id="in"  {...input} style={{width: size || '70%', display: 'block'}} />
        { (touched && error) && <Message style={{marginLeft: 4, fontWeight: 'bold',}} severity="error" text={error} />  }
    </div>
)

export const InputArea = ({ input, label, meta: { touched, error }, })  => (
    <div>
        <label>{label}</label><br />
        <InputTextarea {...input}  maxLength={200} rows={5} cols={30} value={input.value} onChange={input.onChange} />
        { (touched && error) && <Message style={{marginLeft: 4, fontWeight: 'bold',}} severity="error" text={error} /> }
    </div>
)

export const InputSelectOption = ({ input, label, list, meta: { touched, error }, })  => (
    <div>
        <label>{label}</label><br />
        <Dropdown {...input} itemTemplate={itemTemplate} required disabled={list.length === 0} options={list} 
        placeholder="Selecione" optionLabel="name" filter={true} 
        filterBy="label,value" showClear={true} />

       { (touched && error) && <Message style={{marginLeft: 4, fontWeight: 'bold',}} severity="error" text={error} /> }
    </div>
)

export const InputMoney = ({ onHandleValue ,input, meta: { touched, error }, })  => (
    <div>
        <CurrencyInput  {...input} className={'inputMoney'} prefix="R$" decimalSeparator="," 
            thousandSeparator="." onChangeEvent={onHandleValue}   />
        { (touched && error) && <Message style={{marginLeft: 4, fontWeight: 'bold',}} severity="error" text={error} /> }
    </div>
)

export const InputWithMask = ({ input, size, label, meta: { touched, error }}) => (
    <div>
         <label>{label}</label><br />
        <InputMask {...input} mask={'(99) 9999-99999'} maxlength={11} style={{width: size || '70%', display: 'block'}} />
        { (touched && error) && <Message style={{marginLeft: 4, fontWeight: 'bold',}} severity="error" text={error} /> }
    </div>
)

function itemTemplate(option) {
    //console.log(option)
    return(
        <span key={option.id} >{option.name}</span>
    )
}