import React,{ Component } from 'react'
import { Link } from 'react-router-dom'

export default class TopBar extends Component {

    state = {

    }

    render() {


        return(
            <div>
                
                <div className={'topbar'} style={{display: 'flex', justifyContent: 'space-between'}}>
                    <Link to={"/"} style={{color: '#fff'}}>
                        <i className="pi pi-home" style={{ marginLeft: 6 }}> </i>
                    </Link>

                    <div style={{ justifyContent: 'flex-end', alignItems: 'center'}}>
                        <Link to={'/debts'}>
                            <i className="pi pi-list" style={{ marginRight: 6 }} ></i>
                        </Link>
                        <Link to={'/people'} style={{color: '#fff'}}>
                            <i className="pi pi-users"  style={{ marginRight: 6 }} ></i>
                        </Link>
                        <Link to={'/products'} style={{color: '#fff'}}>
                            <i className="pi pi-tablet"  style={{ marginRight: 6 }} ></i>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

