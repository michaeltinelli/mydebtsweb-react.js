import React, { Component } from 'react';
import './App.css';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';


class App extends Component {

  state = {
      products: [
        { id: 1, name: 'Cigarro Varejo', price: 1.25, category: 'Cigarros' },
        { id: 2, name: 'Cerveja Cintra', price: 5.50, category: 'Bebidas' },
        { id: 3, name: 'Trakinas', price: 2.10, category: 'Biscoitos' },
        { id: 4, name: 'Saco de batatas', price: 8.50, category: 'Frutas' },
        { id: 5, name: 'Coca-Cola 2 L', price: 10.00, category: 'Bebidas' },
      ]
  }

  render() {
    return (
        <div className="App">
          <h4>Dívidas adicionadas hoje</h4> 
          <DataTable value={this.state.products} paginator={true} rows={5}>
            <Column field="name" header="Nome" />
            <Column field="category" header="Categoria" />
            <Column field="price" header="Preço" />
          </DataTable>
        </div>
    );
  }
}

export default App;
