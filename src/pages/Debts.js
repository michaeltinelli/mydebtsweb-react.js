import React,{ Component } from 'react'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { connect } from 'react-redux'
import {Panel} from 'primereact/panel';
import {Growl} from 'primereact/growl';
import CurrencyInput from 'react-currency-input';
import { Link } from 'react-router-dom'

class Debts extends Component {


    render() {
      return (
        <div>
            <Panel header={'Dívidas cadastradas'}>
              <Growl  ref={(el) => this.growl = el} />
              <Link to={"/debts/newdebt"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                  <span style={{marginRight: 4, }}>Nova dívida</span>
                  <br />
              </Link>   
            </Panel>
        </div>
      )
    };
}

const mapStateToProps = ({ debts }, ownProps) => {
    return {
        debts: debts.debts
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Debts)