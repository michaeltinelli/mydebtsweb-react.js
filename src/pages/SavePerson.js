import React,{ Component } from 'react'
import {Panel} from 'primereact/panel';
import { Button } from 'primereact/button'
import CurrencyInput from 'react-currency-input';
import { InputDefault, InputWithMask } from '../templates/inputFields';
import { Field, reduxForm, reset } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {Growl} from 'primereact/growl';
import { newPerson, updatePerson, loadPerson } from '../store/actions/peopleActions';

class SavePerson extends Component {

    componentDidMount() {
        console.log(this.props.match.params.personId)
        if(this.props.match.params.personId) {
            this.props.onLoadPerson(this.props.match.params.personId)
        }
    }

    showMessage() {
        this.growl.show({severity: this.props.isEdit ? 'info' : 'success', summary: this.props.isEdit ? 
        'Dados alterados.' : 'Pessoa cadastrada', detail: this.props.isEdit ? 
        'Dados da pessoa salvo com sucesso' : 'Pessoa cadastrada com sucesso.'});
    }

    save = values => {
        const person = {
            id: values.id || '',
            name: values.name || '',
            cel: values.cel || '',
            email: values.email || '',
            createdAt: values.createdAt || new Date(),
        }
        if(this.props.isEdit) {
            this.props.onUpdatePerson(person)
        } else {
            this.props.onSavePerson(person)
        }
    }

    render() {

        const { handleSubmit, valid, isEdit, header, submitSucceeded, reset } = this.props

        if(submitSucceeded && !this.props.isEdit) {
            reset()
            this.showMessage()
        } else if(submitSucceeded && this.props.isEdit) {
            this.showMessage()
        }


        return(
            <div>
                <Growl  ref={(el) => this.growl = el} />
                <Panel header={header}>
                    <Link to={"/people"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                        <span style={{marginRight: 4, }}>Listagem de Pessoas</span>
                    </Link>
                    <form>
                        <Field name={'name'} label={'Nome'} component={InputDefault} />
                        <Field name={'cel'} label={'Cel:'} component={InputWithMask} />
                        <Field name={'email'} label={'E-mail:'} component={InputDefault} /><br />
                        <Button disabled={!valid} onClick={handleSubmit(this.save)} 
                         className={`p-button-${isEdit ? 'info' : 'success'} p-button-raised`}   label={isEdit ? 'Salvar' : 'Cadastrar'} />  
                    </form>
                </Panel>
            </div>
        )
    }
}

const validate = values => {
   
    const errors = {}

    if(!values.name) {
        errors.name = 'Campo nome é obrigatório'
    } else if(values.name.length < 4 || values.name.length > 150) {
        errors.name = 'Preecha entre 4 e 150 caracteres.'
    }

   
    if( values.cel && values.cel.length < 11 ) {
        errors.cel = 'Preecha até 11 caracteres.'
    }

    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Email inválido.'
    }
   
    return errors
}

SavePerson = reduxForm({form: 'SavePersonForm', validate })(SavePerson)

const mapStateToProps = ({ people }, ownProps) => {
    return {
        person: people.person,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSavePerson: person => dispatch(newPerson(person)),
        onUpdatePerson: person => dispatch(updatePerson(person)),
        onLoadPerson: person => dispatch(loadPerson(person))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(SavePerson)