import React,{ Component } from 'react'
import {Panel} from 'primereact/panel';
import { Button } from 'primereact/button'
import CurrencyInput from 'react-currency-input';
import { InputDefault, InputArea, InputSelectOption, InputMoney } from '../templates/inputFields';
import { Field, reduxForm, reset } from 'redux-form'
import { connect } from 'react-redux'
import { InputText } from 'primereact/inputtext';
import {Dialog} from 'primereact/dialog';
import {Growl} from 'primereact/growl';
import { newCategory, listCategories } from '../store/actions/categoryActions';
import { newProduct, loadProduct, updateProduct, } from '../store/actions/productActions';
import { Link } from 'react-router-dom'

class SaveProduct extends Component {

    componentDidMount = () => {
        console.log(this.props)
        this.props.onGetCategories()
        if(this.props.match.params.productId) {
            this.props.onLoadProduct(this.props.match.params.productId)
        }
        
    }

    state = {
        visible: false,
        priceNumber: 0,
        nameCategory: '',
    }


    save = values => {
        const product = {
            id: values.id || '',
            name: values.name,
            description: values.description || '',
            price: this.state.priceNumber,
            category: values.category,
            updateAt: new Date()
        }
        if(this.props.isEdit) {
            this.props.onUpdateProduct(product)
        } else {
            this.props.onNewProduct(product)
        }
    }

    handleValue = (event, maskedvalue, value) => {
        this.setState({priceNumber: value})
    }

    dialogCategory = () => {

        return (
            <Dialog header="Nova categoria" visible={this.state.visible} style={{}} 
                modal={true} onHide={() => this.setState({visible: false})}>
                
                <div style={{justifyContent: 'flex-start', display: 'flex'}}>
                    <span className="p-float-label">
                        <InputText id="float-input" type="text" size={'20%'} value={this.state.nameCategory} 
                            onChange={(e) => this.setState({nameCategory: e.target.value})} />
                        <label htmlFor="float-input">Categoria</label>
                    </span>
                    <Button disabled={this.state.nameCategory.length < 4} style={{marginLeft: 4}} 
                       onClick={() => this.saveNewCategory()} label="Salvar" icon="pi pi-check" iconPos="right" />
                </div>
               
            </Dialog>
        )
    }

    saveNewCategory = () => {
        const category = {
            id: '',
            name: this.state.nameCategory
        }
        this.props.onNewCategory(category)
        this.setState({visible: false, nameCategory: ''})
    }
   
    showMessage() {
        this.growl.show({severity: this.props.isEdit ? 'info' : 'success', summary: this.props.isEdit ? 
        'Dados alterados.' : 'Produto cadastrado', detail: this.props.isEdit ? 
        'Dados do produto salvo com sucesso' : 'Produto cadastrado com sucesso.'});
    }

    render() {

        const { handleSubmit, valid, submitSucceeded, reset, isEdit } = this.props

        if(submitSucceeded && !this.props.isEdit) {
            reset()
            this.showMessage()
        } else if(submitSucceeded && this.props.isEdit) {
            this.showMessage()
        }

        return(
            <div>
                <Growl  ref={(el) => this.growl = el} />
                <Panel header={this.props.header} className={'formProduct'}>
                <Link to={"/products"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                    <span style={{marginRight: 4, }}>Listagem de Produtos</span>
                </Link>
                {this.state.visible && this.dialogCategory()}
               
                    <form role='form'>
                        <Field name={'name'} label={'Nome'} component={InputDefault} />
                        <Field name={'description'} label={'Descrição'} component={InputArea} />

                        <div style={{ display: 'flex' ,justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Field name={'category'}  list={this.props.categories} label={'Categoria'} 
                                component={InputSelectOption} />
                            <i style={{marginLeft: 4}}
                                onClick={() => this.setState({visible: true})} className="pi pi-plus"></i>
                        </div>

                        <Field name={'price'} onHandleValue={this.handleValue} component={InputMoney} />
                        <Button label={isEdit ? 'Alterar' : 'Cadastrar'} onClick={handleSubmit(this.save)} disabled={!valid}
                            style={{marginTop: '6px'}} className={`p-button-${isEdit ? 'info' : 'success'} p-button-raised`} />
                    </form>
                </Panel>
            </div>
        )
    }
}

const validate = values => {
    //console.log(values)
    const errors = {}

    if(!values.name) {
        errors.name = 'Campo Nome obrigatório.'
    } else if(values.name.length < 4 || values.name.length > 30) {
        errors.name = 'Digite entre 4 e 30 letras.'
    }

    if(values.description && (values.description.length < 8 || values.description.length > 200)) {
        errors.description = 'Digite entre 8 e 200 letras.'
    }

    if(!values.category) {
        errors.category = 'Selecione uma categoria.'
    }

    if(!values.price) {
        errors.price = 'Campo preço obrigatório.'
    }

    return errors
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onNewCategory: category => dispatch(newCategory(category)),
        onGetCategories: () => dispatch(listCategories()),
        onNewProduct: product => dispatch(newProduct(product)),
        onLoadProduct: id => dispatch(loadProduct(id)),
        onUpdateProduct: product => dispatch(updateProduct(product))
    }
}

const mapStateToProps = ({ prod }, ownProps) => {
    return {
       categories: prod.categories,
    }
}

SaveProduct = reduxForm({form: 'SaveProductForm', validate })(SaveProduct)

export default connect(mapStateToProps, mapDispatchToProps)(SaveProduct)