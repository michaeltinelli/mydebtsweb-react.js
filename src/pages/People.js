import React,{ Component } from 'react'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { connect } from 'react-redux'
import {Panel} from 'primereact/panel';
import {Growl} from 'primereact/growl';
import CurrencyInput from 'react-currency-input';
import { Link } from 'react-router-dom'
import { loadPeople } from '../store/actions/peopleActions';

class People extends Component {

    componentDidMount = () => {
        this.props.onListPeople()
    }

    render() {

        return(
            <div>
                <Panel header="Pessoas Cadastradas">
                <Growl  ref={(el) => this.growl = el} />
                    <Link to={"/people/newperson"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                        <span style={{marginRight: 4, }}>Nova pessoa</span>
                    </Link>
                    <br />
                    <DataTable value={this.props.people} emptyMessage={'Não há pessoas cadastradas'} paginator={true} rows={5}>
                        <Column header={'Nome'} field={'name'}/>
                        <Column  body={(e) => {
                            return (
                                <CurrencyInput prefix={'R$'} decimalSeparator="," 
                                    thousandSeparator="." style={{fontWeight: 'bold', width: '80%'}} 
                                    disabled value={e.price || 0} />
                            )
                        }}  header="Está devendo" />
                        <Column header="Ações"  body={(e) => {
                            return(
                                <div style={{textAlign: 'right'}}>
                                    <i className="pi pi-list" style={{fontSize: '25px', color: 'green', marginLeft: 4}}></i>
                                    <Link to={`/people/${e.id}`}><i className="pi pi-pencil" 
                                        style={{fontSize: '25px',color: 'black'}}></i>
                                    </Link>
                                    <i className="pi pi-trash" style={{fontSize: '25px', color: 'red', marginLeft: 4}}></i>
                                </div>
                            )
                        }} />
                    </DataTable>
                </Panel>
            </div>
        )
    }
}

const mapStateToProps = ({ people }, ownProps) => {
    return {
        people: people.people,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
       onListPeople: () => dispatch(loadPeople()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(People)