import React,{ Component } from 'react'
import {Panel} from 'primereact/panel';
import { Button } from 'primereact/button'
import CurrencyInput from 'react-currency-input';
import { InputDefault, InputArea , InputSelectOption} from '../templates/inputFields';
import { Field, reduxForm, reset } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Growl } from 'primereact/growl';
import {loadPeople} from '../store/actions/peopleActions'
import {listProducts} from '../store/actions/productActions'


class SaveDebt extends Component {

    componentDidMount() {
        this.props.onLoadPeople()
        this.props.onLoadProducts()
        
    }

    render() {

        return(
            <div>
                <Panel header={this.props.header}>
                <Growl  ref={(el) => this.growl = el} />
                    <Link to={"/debts"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                        <span style={{marginRight: 4, }}>Listagem de Dívidas</span>
                    </Link>
                    <form>
                        <Field name={'name'} label={'Nome'} component={InputDefault} />
                        <Field name={'description'} label={'Descrição'} component={InputArea} />
                        <Field name={'people'} label={'Pessoa'} list={this.props.people} component={InputSelectOption} /><br />
                        <Field name={'product'} label={'Produto'} list={this.props.products} component={InputSelectOption} />
                    </form>
                </Panel>
            </div>
        )
    }
}

SaveDebt = reduxForm({form: 'SaveDebtForm', })(SaveDebt)

const mapStateToProps = ({ people, prod }, ownProps) => {
    return {
        people: people.people,
        products: prod.products,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadPeople: () => dispatch(loadPeople()),
        onLoadProducts: () => dispatch(listProducts()),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(SaveDebt)