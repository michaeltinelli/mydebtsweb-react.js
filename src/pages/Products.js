import React,{ Component } from 'react'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { connect } from 'react-redux'
import {Panel} from 'primereact/panel';
import { listProducts, removeProduct } from '../store/actions/productActions';
import CurrencyInput from 'react-currency-input';
import {Growl} from 'primereact/growl';
import { Link } from 'react-router-dom'

class Products extends Component {

    state = {
       
    }

    componentDidMount = () => {
        this.props.onListProducts()
    }

    renderDataTable = () => {
        
        return (
            <DataTable value={this.props.products}   paginator={true} rows={5}>
                <Column field="name" header="Nome" />
                <Column style={{width: '50%'}} body={(e) => {
                    return (
                        <CurrencyInput prefix={'R$'} decimalSeparator="," 
                            thousandSeparator="." style={{width: '95%', fontWeight: 'bold',}} 
                            disabled value={e.price} />
                    )
                }}  header="Preço p/unidade" />
                <Column header="Ações"  body={(e) => {
                    
                    return(
                        <div style={{textAlign: 'right'}}>
                           <Link to={`/product/${e.id}`}><i className="pi pi-pencil" 
                                style={{fontSize: '25px',color: 'black'}}></i></Link>
                           <i className="pi pi-trash"onClick={() =>  this.props.onRemoveProduct(e.id)} style={{fontSize: '25px', color: 'red', marginLeft: 4}}></i>
                        </div>
                    )
                }} />
            </DataTable>
        )
    }

  
    render() {


        return(
            <div>
                <Panel header="Produtos Cadastrados">
                <Growl  ref={(el) => this.growl = el} />
                    <Link to={"/products/newProduct"} style={{display: 'flex',justifyContent: 'flex-end'}}>
                        <span style={{marginRight: 4, }}>Novo produto</span>
                    </Link>
                    <br />
                    {this.renderDataTable()}
                </Panel>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onListProducts: () => dispatch(listProducts()),
        onRemoveProduct: id => dispatch(removeProduct(id)),
    }
}

const mapStateToProps = ({prod}, ownProps) => {
    return {
        products: prod.products,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)